# Guildline for set up environment

prior run to the application please setup enviroment as follows
 >install node js
 >typescript
 >nodemon as gloable level

then install package as bellow
>npm i


inorder to connect to mysql database

format your connection string accordingly.

    await createConnection({
        type: "mysql",
        host: "db4free.net",
        database: "sampleproductdb",
        username: "mysqlsampleuser",
        password: "1qaz2wsx@",
        logging: true,
        synchronize: false,
        entities: [Products]
    });

after set the connection string 

    await createConnection({
      ...
        synchronize: true,
      ...
    });

set the    synchronize: true, and then save change and wait for db auto generate happen.
after generating compleate you should need to set synchronize: false .

 after that project can be run as
 >npm run dev

restart when you do any changes / disconnect from internet please rerun the server.

if you run project successfully follwing message should come.

>\product-api> npm run dev

> api_typescript@1.0.0 dev C:\Work\GapStar\api-assessment\product-api
> nodemon ./src/index.ts

>[nodemon] 2.0.7
>[nodemon] to restart at any time, enter `rs`
>[nodemon] watching path(s): *.*
>[nodemon] watching extensions: ts,json
>[nodemon] starting `ts-node ./src/index.ts`
>SERVER IS RUNNIG ON PORT 3001

# assingment notes:
(1.0)
(Here currently used free account for mysql online.
in a case where if that expired you can auto generate DB as instruct.)


normal API url :
http://localhost:3001/api/*

(2.a)
http://localhost:3001/api/products
httpGet Request

(2.b)
http://localhost:3001/api/product/1
{product by id}

(2.c)
http://localhost:3001/api/product/
httpPost Request
sample upload format
{
    "name": "testnew name",
    "slug": "slug001",
    "sku": "sku001",
    "brand": "tst brand"
}

(2.d)
http://localhost:3001/api/product/15
httpDelete Request

(2.e)

http://localhost:3001/api/product
httpPut Request
sample upload format   
    {
        "id": 1,
        "name": "sample Product Name",
        "slug": "slug",
        "sku": "sku",
        "brand": "brand3333333333333"
    }

(2.f)

please make sure to include nessary permision to to project to file upload to work.

upload:
http://localhost:3001/api/product/upload

comma seperated csv file format should be as below:

name,brand,sku,slug
name1,brand1,sku1,slug1
name2,brand2,sku2,slug2

when posting use "path" as json property in file
as demostrated in Screen\upload.JPG


(3.0)

graphql URL http://localhost:3001/graphql
document contains mutation and the query sample.




