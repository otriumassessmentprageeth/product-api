import { Products } from "../Entities/Products";
import { ProductDetail } from "../Interfaces/ProductDTO";


export class ProductService {

    async getAllProducts() {
        return await Products.find();
    }
    async getProduct(id: any) {
        return await Products.findOne(id);
    }
    async createProduct(args: ProductDetail) {
        return await Products.insert(args);
    }
    async updateProduct(args: ProductDetail) {
        return await Products.update(args.id, args);
    }
    async deleteProduct(id: any) {
        return await Products.delete(id);
    }
    async uploadProducts(file: any) {
        let csvData = file.path.data.toString('utf8');
        //   let fs = require('fs');
        const csv = require('csvtojson')
        var products = await csv().fromString(csvData);
        var promArray: any = [];

        products.forEach((element: any) => {
            var pr = new Products();
            pr.brand = element.brand;
            pr.name = element.name;
            pr.slug = element.slug;
            pr.sku = element.sku;

            var prodCreateProm = this.createProduct(pr);
            promArray.push(prodCreateProm)
        });

        await Promise.all(promArray);
    };

}