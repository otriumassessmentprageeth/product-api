import { Application, Request, Response, Router } from 'express';
import express from 'express';
import { ProductService } from "../Services/ProductService";


export class ProductRoute {

    app: express.Application;
    productService: ProductService;

    constructor(app: express.Application) {
        this.app = app;
        this.productService = new ProductService();
    }

    configRoutes() {

        this.app.route("/api/products")
            .get(async (req: express.Request, res: express.Response) => {
                let list = await this.productService.getAllProducts();
                res.status(200).send(list);
            });

        this.app.route("/api/product/:productId")
            .get(async (req: express.Request, res: express.Response) => {
                let id = req.params.productId;
                let prod = await this.productService.getProduct(id);
                res.status(200).send(prod);
            });

        this.app.route("/api/product")
            .post(async (req: express.Request, res: express.Response) => {
                let body = req.body;
                let createdProd = await this.productService.createProduct(body);
                res.status(200).send(createdProd);
            });

        this.app.route("/api/product")
            .put(async (req: express.Request, res: express.Response) => {
                let body = req.body;
                let producUpdated = await this.productService.updateProduct(body);
                res.status(200).send(producUpdated);
            });

        this.app.route("/api/product/:productId")
            .delete(async (req: express.Request, res: express.Response) => {
                let id = req.params.productId;
                await this.productService.deleteProduct(id);
                res.status(200).send('true');
            });

            this.app.route("/api/product/upload")
            .post(async (req: any, res: express.Response) => {
               await this.productService.uploadProducts( req.files);
               res.status(200).send('true');
            });

        return this.app;

    }
}