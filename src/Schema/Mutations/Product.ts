import { GraphQLString } from "graphql";
import { resolve } from "node:path";
import { ProductType } from "../TypeDefs/Product";
import { Products } from "../../Entities/Products"

export const CREATE_PRODUCT = {
    type: ProductType,
    args: {
        name: { type: GraphQLString },
        slug: { type: GraphQLString },
        sku: { type: GraphQLString },
        brand: { type: GraphQLString }
    },
    async resolve(parent: any, args: any) {
        const { name, slug, brand, sku } = args;
        await Products.insert({ name, sku, brand, slug });
        return args;
    }

}