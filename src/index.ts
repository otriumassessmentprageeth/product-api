import express from "express";
import { graphqlHTTP } from "express-graphql";
import cors from "cors"
import { createConnection } from "typeorm";
import { schema } from "./Schema"
import { Products } from "./Entities/Products"
import { ProductRoute } from "./Routes/ProductRoutes";
import { FileArray, UploadedFile, Options } from "express-fileupload";
// import {} from 'path'
import fileUpload = require('express-fileupload');

const main = async () => {

    await createConnection({
        type: "mysql",
        host: "db4free.net",
        database: "sampleproductdb",
        username: "mysqlsampleuser",
        password: "1qaz2wsx@",
        logging: true,
        synchronize: false,
        entities: [Products]
    });

    const app = express();
    app.use(cors());
    app.use(fileUpload({
        createParentPath: true,
        tempFileDir: '/tmp/'
    }));
    app.use(express.json())

    // app.use(UploadedFile({
    //     useTempFiles : true,
    //     tempFileDir : path.join(__dirname,'tmp'),
    // }));
    // app.use(fileUpload({
    //     useTempFiles : true,
    //     tempFileDir : '/tmp/'
    // }));


    app.use("/graphql", graphqlHTTP({
        schema,
        graphiql: true
    }));

    (new ProductRoute(app)).configRoutes();

    app.listen(3001, () => {
        console.log("SERVER IS RUNNIG ON PORT 3001")
    });

};

main().catch((err) => {
    console.log(err);
});