export interface ProductDetail {
    id:number
    name: string
    slug: string
    brand: string
    sku: string
}